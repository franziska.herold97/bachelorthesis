function A = chebyshev_mod(x, s, N)
    %calculate T_s(x) mod(N) recursive
    %basic  cases
    T0 = 1;
    T1 = mod(x, N);
    % If s == 0, get T0
    if s == 0
        A = T0;
        return;
    end
    % if s == 1, get T1
    if s == 1
        A = T1;
        return;
    end
    %Iterative calculation for s > 1
    for i = 2:s
        % Calculate T_i(x) = (2 * x * T_(i-1)(x) - T_(i-2)(x)) mod N
        T_current = mod(2 * mod(x * T1, N) - T0, N);
        
        % Update T0 and T1 
        T0 = T1;
        T1 = T_current;
    end
    %calcualted value of the polynomial
    A = T1;
end