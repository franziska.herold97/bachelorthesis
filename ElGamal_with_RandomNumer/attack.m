function hacked_plaintext = attack(public_key, ciphertext)

%Obtain Alices authentic public key (x, N, A)
x = public_key(1)
N = public_key(2);
A = public_key(3);
B = ciphertext(1)
X = ciphertext(2);

% brute-force attack
function r_cracked = crack_chebyshev(N, B, max_r)
    for r_guess = 0:max_r
        %if mod(cosh(r_guess * acosh(A)), p) == B
        if chebyshev_mod(x, r_guess, N) == B    
            r_cracked = r_guess;
            return;
        end
    end
    error('Private key not found within the range.');
end

max_r = N-1; %max number of brute force attack runs
r_cracked = crack_chebyshev(N, B, max_r); %call the brute force function

disp(['Cracked Private Key r: ', num2str(r_cracked)]);

%calculate c with cracked r
C = chebyshev_mod(A, r_cracked, N);
mod_C = modInv(C, N);

%calcualte the hacked_plaintext
hacked_plaintext = mod(mod_C * X, N);

end