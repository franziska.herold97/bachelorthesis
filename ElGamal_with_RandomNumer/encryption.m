function [ciphertext, m] = encryption(public_key)

%1. Obtain Alices authentic public key (x, N, A)
x = public_key(1);
N = public_key(2);
A = public_key(3);

% 2. Represent the message as an integer m in the range {0,1,..., N-1}

%an random integer m such that m < N
m = randi([1, N-1]);

%3. Select random integer r < N

%random integer r such that r < N
r = randi([1, N-1])


%4. Compute B = T_r_x(mond N) and X = m T_r_A(mod N)

B = chebyshev_mod(x, r, N);

T_r_A = chebyshev_mod(A, r, N);
X = mod(m* T_r_A, N);


%5. Send the cipher-text c = (B, X) to Alice
ciphertext = [B, X];

end