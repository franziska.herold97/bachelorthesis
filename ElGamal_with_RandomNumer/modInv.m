function x_inv = modInv(x,p)
%computes the modular inverse of x modulo p
%x and p must not have common divisors!

x_inv = 0;

[g,c,~] = gcd(x,p);

% c*x + d*p = g, where g is the gcd of x and p.
% This means if g == 1, c is the modular inverse of x (modulo p).

if (g ~=1) %no modular inverse exists
    return;
    
else
    x_inv = c;
    
    if (x_inv < 0) %make sure the inverse is a positive number.
        x_inv = x_inv + p;
    end   
end

