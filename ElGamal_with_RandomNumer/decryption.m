function plaintext = decryption(ciphertext, s, public_key)

%uses private Key s to compute C = T_s_B(mod N)
B = ciphertext(1);
N = public_key(2);
X = ciphertext(2);

%calculate C
C = chebyshev_mod(B, s, N);

%recover m by computing m = X * inv(C) (mod N)
mod_C = modInv(C, N);
plaintext = mod(mod_C * X, N);
disp("plaintext");
disp(plaintext);

end