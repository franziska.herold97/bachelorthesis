function plaintext = attack(public_key, ciphertext)

%publik_key (x, T_s_x)
x = public_key(1);
T_s_x = public_key(2);

%ciphertext C = (T_r_x, X)
T_r_x = ciphertext(1);
X = ciphertext(2);

%1.
%compute an r' such that T_r'_x = T_r_x
%

%Example values
%T_r_x = -0.939692621;
%T_s_x = 0.173648178;
%x = 0.64278761;

%a = round(acos(T_r_x) / acos(x),1)
a = acos(T_r_x) / acos(x)
%a = round(a,1)
%b = round((2 * pi) / acos(x),1)
b = (2 * pi) / acos(x)
%b = round(b,1)

%therefor
%a + b*k_1 = u1
%-a + b*k_2 = u2

%fractional parts
%(a mod 1) + k * (b mod 1) = z
%oder
%-(a mod 1) + k * (b mod 1) = z

function num_decimal_places = countSignificantDecimals(number)
    % convert number to string
    numberStr = num2str(number, '%.16g');
    
    % find dot
    pointIndex = find(numberStr == '.', 1);
    
    if isempty(pointIndex)
        % no decimal places
        num_decimal_places = 0;
    else
        % get the decimal places
        decimalPart = numberStr(pointIndex+1:end);
        
        % clear 0
        decimalPart = regexprep(decimalPart, '0*$', '');
        
        % dezimal places
        num_decimal_places = length(decimalPart);
    end
end

number1 = a;
number2 = b;
numDecimals1 = countSignificantDecimals(number1);
numDecimals2 = countSignificantDecimals(number2);

if numDecimals1 > numDecimals2
    L = numDecimals1
else
    L = numDecimals2
end
%L = numDecimals;
%L =1;
L = 16;

%finite precision implementation
B = 10;

%(a mod 1) * B^L + k * (b mod 1) * B^L = z * B^L
%- (a mod 1) * B^L + k * (b mod 1) * B^L = z * B^L

%therefor
a_ = mod(a,1) * B^L
%a_ = round(a_,1);
b_ = mod(b,1) * B^L
%b_ = round(b_,1);


%therefor
%b_ * k_1 = a_ mod B^L;
%b_ * k_2 = -a_ mod B^L;


if a_ < 0
        a_new = a_ + B^L; 
        %k_2 = mod(round(a_new,1)/b_,B^L /b_);
        k_1 = mod(a_new/b_, B^L/b_); 
        %k_2 = round(k_2,1);
        disp("a_ < 0")
else
    %k_2 = mod(round(a_,1)/b_,B^L /b_);
    k_1 = mod(a_/b_, B^L/b_); 
    %k_2 = round(k_2,1);
    disp("a_ < 0 -> else")
end

if -a_ < 0
        a_new = -a_ + B^L;
        %k_2 = mod(round(a_new,1)/b_,B^L /b_);
        k_2 = mod(a_new/b_,B^L /b_);
        %k_2 = round(k_2,1);
        disp("-a_ < 0")
else
    %k_2 = mod(round(a_,1)/b_,B^L /b_);
    k_2 = mod(a_/b_,B^L /b_);
    %k_2 = round(k_2,1);
    disp("-a_ < 0 -> else")
end

%u_1 = round(-a + b * k_1,1);
u_1 = a + b * k_1;
%u_2 = round(a + b * k_2,1);
u_2 = -a + b * k_2;

%2.
%Evaluate T_r'_s(x) = T_r'(T_s(x)
%
r_1 = u_2
r_2 = u_1

%chebyshev_polynomial1 = chebyshev_recursive(T_s_x, r_1 + 1)
%T_r_1_T_s_x = chebyshev_recursive(T_s_x, r_1)

%T_r_2_T_s_x = chebyshev_recursive(T_s_x, r_2)

T_r_1_T_s_x = cos(r_1 * acos(T_s_x))
T_r_2_T_s_x = cos(r_2 * acos(T_s_x))

%3.
%Recover M = (X / T_r'_s(x))

plaintext_1 = (X / T_r_1_T_s_x)
plaintext_2 = (X / T_r_2_T_s_x)

if round(plaintext_1,10) == round(plaintext_2,10)
    plaintext = plaintext_1;
    disp("Calculation was correct")
else
    plaintext = 0;
    disp("Error in calcluation")
end    
end