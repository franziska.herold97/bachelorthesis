   % Function to calculate Chebyshev-Polynoms T_{n+1}(x) recursive
   %Function to calculate Chebychev-Polynom with degree s at point x using
   %a recursive method
   %T_{n}(x)= 2xT_n(x-1) - T_{n-2}(x).

    function result = chebyshev_recursive(x, s)
        %check if polynomial is degree 0 -> always 1
        if s == 0
            result = ones(size(x));
        %check if polynomial is degree 1 -> always x    
        elseif s == 1
            result = x;
        else
        %compute polynomial for degree s > 1
        %recursive computation of the polynomial
        %T_s(x)= 2xT_s(x-1) - T_{s-2}(x)
            result = 2 * x .* chebyshev_recursive(x, s - 1) - chebyshev_recursive(x, s - 2);
        end
    end