function public_key = generate_public_key(s)

    x = 2 * rand() - 1;  %for one random x between -1 and 1
    %calculate the chebychev polynomial
    chebyshev_polynomial = chebyshev_recursive(x, s + 1); 
    T_s_x = chebyshev_polynomial; % computed T_s(x)
    %public key with a random number and the calculated chebyshev polynomial
    public_key = [x , T_s_x]; 

end






