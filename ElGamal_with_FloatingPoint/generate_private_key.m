function private_key = generate_private_key()
    %number of bits of the random integer value -> greater numbers 
    % -> longer time to calculate the polynom
    num_bits = 5; 
    %generate random bits
    random_bits = randi([0 1], 1, num_bits);   
     %covert the random bits in an integer
    random_integer = bin2dec(char('0' + random_bits)); 
    %for random "big" integer value
    private_key = random_integer; 
end