function C = encrypt_message(public_key, message)

%uses public_key (x,T_s(x))
%represents the message -> message as number M (element between -1 and 1)
%generates large integer r
%number of bits of the random integer value 
%-> greater numbers -> longer time to calculate the polynom
num_bits = 4;  
%generate random bits
random_bits = randi([0 1], 1, num_bits);
 %covert the random bits in an integer
random_integer = bin2dec(char('0' + random_bits));  

r = random_integer; %for random "big" integer value

%%%%
%computes T_r(x), T_r*s(x) = T_r(T_s(x)) and X = X = M * T_r*s(x)  or X = M * T_r(T_s(x)
%%%%

x = public_key(1,1); %x value from public key

%calculate T_r(x)
chebyshev_polynomial = chebyshev_recursive(x, r + 1);
T_r_x = chebyshev_polynomial;

%calculate T_r*s(x) = T_r(T_s(x))
T_s_x = public_key(1,2); % value from public key
chebyshev_polynomial = chebyshev_recursive(T_s_x, r + 1);
T_r_T_s_x = chebyshev_polynomial;

% calculate X = M * T_r*s(x)  or X = M * T_r(T_s(x)

X = message * T_r_T_s_x;

%%%%%
%Calculate ciphertext C = (T_r(x),X)
%%%%%

C = [T_r_x, X];

end