%main.m
%call functions for encryption and sending key and message
clear all

format long;
R = 2; %Value for round()

%Alice
%generate private key
private_key = generate_private_key();
%private_key = round(private_key, R);

%Alice
%call function generate public and private key
public_key = generate_public_key(private_key);

%Bob
%encrypt message with public key from Alice
message = 0.3465;
%message = 1;
ciphertext = encrypt_message(public_key, message);

%Alice
%decrypt the ciphertext message from bob with her private key
decrypted_message = decrypt_message(private_key, ciphertext);

%compare message and decrypted_message
if(round(message,R) == round(decrypted_message,R))   %round due to calculation errors (deviations in the 16 decimal place)
    disp("The decrypted message is the same as the original message")
else
    disp("The messages are not equal!")
end

%Attack
%Call the attack function
%with M and public_key
plaintext = attack(public_key, ciphertext);
disp("Plaintext:");
disp(plaintext);

if round(plaintext,10) == round(message,10)
     disp("Plaintext is equal to the message")
else
     disp("Error")
 end    


