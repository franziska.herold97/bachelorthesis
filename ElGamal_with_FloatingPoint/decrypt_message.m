function M = decrypt_message(private_key, C)

%Divide matrix C in T_r(x) and the private key s
T_r_x = C(1, 1);
X = C(1, 2);
s = private_key;

%Uses her private key s to compute T_s*r(x) = T_s(T_r(x))
chebyshev_polynomial = chebyshev_recursive(T_r_x, s + 1);
T_s_T_r_x = chebyshev_polynomial;

%Recovers M by computing M = X / T_s*r(x)
M = X / T_s_T_r_x;

end