% Plot of 5 chebyshev polynomials of the first kind
x = -1:.01:1;
T = @(x,n) cos(n*acos(x));  % Generating function.

plot(x,T(x,0),'r',x,T(x,1),'b',x,T(x,2),'k',x,T(x,3),'g',x,T(x,4),'c')
axis([-1.2 1.2 -1.2 1.2])
grid on
legend({'Ch_0';'Ch_1';'Ch_2';'Ch_3';'Ch_4'})
ylabel('T_n(x)')
xlabel('x')
legend('T_0(x)','T_1(x)','T_2(x)','T_3(x)','T_4(x)','Location','Best')
title('Chebyshev polynomials of the first kind')

